
db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
})

/*
{
    "acknowledged" : true,
    "insertedId" : ObjectId("643e86c1646ff486c1199aaa")
}
*/

db.rooms.insertMany([
	{
		name: "double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accommodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
])

/*
{
    "acknowledged" : true,
    "insertedIds" : [ 
        ObjectId("643e887f646ff486c1199aab"), 
        ObjectId("643e887f646ff486c1199aac")
    ]
}
*/

db.rooms.find({name: "double"});

/*
{
    "_id" : ObjectId("643e887f646ff486c1199aab"),
    "name" : "double",
    "accommodates" : 3.0,
    "price" : 2000.0,
    "description" : "A room fit for a small family going on a vacation",
    "rooms_available" : 5.0,
    "isAvailable" : false
}
*/

db.rooms.updateOne(
	{name: "queen"},
	{
		$set: {
			rooms_available: 0
		}
	}
)

/*
{
    "acknowledged" : true,
    "matchedCount" : 1.0,
    "modifiedCount" : 1.0
}
*/

db.rooms.deleteMany({rooms_available: 0});
/*
{
    "acknowledged" : true,
    "deletedCount" : 1.0
}
*/