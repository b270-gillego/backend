
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(response => {
	const titles = response.map(post => post.title)
	console.dir(titles)
})

fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(response => console.log(response))


  fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(todo => console.log(`The item ${todo.title} on the list has a status of ${todo.completed}`));




fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		body: "Hello World",
		userId: 1
	})
})
.then(response => response.json())
.then(response => console.log(response));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with different data structure",
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(response => console.log(response));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		status: "Complete",
		title: "delectus aut autem"
	})
})
.then(response => response.json())
.then(response => console.log(response));



fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})
.then(response => response.json())
.then(response => console.log(response))


