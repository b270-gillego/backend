// console.log("Happy Thursday, Batch 270!");

// Function Parameters and Arguments

	// We learned that we can gather data from user input using prompt()

	function printInput(){
		let nickname = prompt("Enter your nickname:");
		console.log("Hi, " + nickname);
	}
	// printInput();

	// Consider this function:

	function printName(name){
		console.log("My name is " + name);
	}
	printName("Juana");
	printName("John");
	printName("Jane");

	// "name" - parameter
	// A parameter acts as a container/named variable that exists only inside a function
	// "Juana" - argument
	// An argument is the information/data provided directly into the function

	let sampleVariable = "Yui";

	printName(sampleVariable);

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);

		let isDivisibleby8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleby8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);

	// Function as Arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.")
	}

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	invokeFunction(argumentFunction);

	console.log(argumentFunction);

	// Multiple Parameters

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	}

	createFullName("Juan", "Dela", "Cruz", "JR");

	//  In JS, providing more/less arguments than the expected parameters will not return an error

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";
	createFullName(firstName, middleName, lastName);

// Return Statement

	function returnFullname(firstName, middleName, lastName){

		console.log("This message will not be printed.")
		return firstName + " " + middleName + " " + lastName;
		// any line/block of code that comes after the return statement is ignored because it ends the function execution
		
	}

	// Whatever value is returned from the returnFullName function is stored in the completeName variable
	let completeName = returnFullname("Jeffrey", "Smith", "Bezos");
	console.log(completeName);

	// You can also create a variable inside the function to contain the result and return that variable instead.
	function returnAddress(city, country){
		let fullAddress = city + "," + country
		return fullAddress;
	}

	let myAddress = returnAddress("Cebu City", "Philippines");
	console.log(myAddress);

	function printPlayerInfo(username, level, job){
		console.log("Username: " + username);
		console.log("Level: " + level);
		console.log("Job: " + job);
	}

	let user1 = printPlayerInfo("knight_white", 95, "Paladin");
	console.log(user1); //returns undefined because printPlayerInfoi returns nothing.  It only console.logs the details.

	//You cannot save any value from printPlayerInfo() because it does not return anything.

	//Other sample
	//THis functtion will only print the sum of num1 + num2
	
	function printSum(num1, num2){
		return num1 + num2; //10
	}
	// printSum(5, 5);

	function getProduct(num){
		let product = num * 5;
		console.log(product);
	}//10

	getProduct(printSum(5, 5));


	function getDifference(num1, num2){
		return num1 - num2; //returns the difference of num1 and num2
	}

	let difference = getDifference(10, 5); //assigns the returned value to the variable "difference"
	console.log(difference);

	function getQuotient(num){
		let quotient = num / 2;
		console.log(quotient);
	}
	getQuotient(difference);