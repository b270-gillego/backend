// console.log("Hello World");

const num = 2
let getCube = num ** 3
console.log(`The cube of ${num} is ${getCube}`);

let address = [258, "Washington Ave NW", "California", 90011];

let [houseNum, street, state, zip] = address;

console.log(`I live at ${houseNum} ${street}, ${state} ${zip}`)

let animal = {
	name: "Lolong",
	habitat: "saltwater",
	kind: "crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in"
}

let {name, habitat, kind, weight, length} = animal;

console.log(`${name} was a ${habitat} ${kind}.  He weighed at ${weight} with a measurement of ${length}.`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach(num => console.log(num));

let reduceNumber = numbers.reduce((x, y) => x + y);

console.log(reduceNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);