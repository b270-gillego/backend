// console.log("Hello World!");

let num1 = 25;
let num2 = 5;
let num3 = 156;
let num4 = 44;
let num5 = 17;
let num6 = 10;

let sum = num1 + num2;
console.log("The result of num1 + num2 should be 30.");
console.log("Actual Result:");
console.log(sum);

let sum2 = num3 + num4;
console.log("The result of num3 + num4 should be 200.");
console.log("Actual Result:");
console.log(sum2);

let difference = num5 - num6;
console.log("The result of num5 - num6 should be 7.");
console.log("Actual Result:");
console.log(difference);

let minutesHour = 60;
let hoursDay = 24;
let daysWeek = 7;
let weeksMonth = 4;
let monthsYear = 12;
let daysYear = 365;
let minutes = daysYear * hoursDay * minutesHour;
console.log("There are " + minutes + " minutes in a year.");

let tempCelsius = 132;
const CelToFaren = 9/5;
const toFarenValue = 32;
let CeltoFarenResult = tempCelsius * CelToFaren + toFarenValue
console.log(tempCelsius + " degrees Celsius when converted to Farenheit is " + CeltoFarenResult);

let num7 = 165;
let divisor = 8;
let modulo = 165 % 8;
console.log("The remainder of " + num7 + " divided by 8 is: " + modulo);

console.log("Is num7 divisible by 8?");
let isDivisibleBy8 = modulo % 8;
console.log(isDivisibleBy8 === 0);

let num8 = 348;
let isDivisibleBy4 = num8 % 4;
console.log("The remainder of " + num8 + " divided by 4 is: " + isDivisibleBy4);

console.log("Is num8 divisible by 4?");
console.log(isDivisibleBy4 === 0);
